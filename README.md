# VLC Plot Analyzer

This repository provides tools to debug the timestamps and events from
the core of VLC:

## vlc_pa.py: log parser to Matplotlib visualizer.

The `vlc_pa.py` script can be used with various sources of the VLC traces (`--tracer` command line argument of VLC).

Examples:

```
$ poetry run vlc-plot-analyzer log_traces.txt
$ cat log_traces.txt | poetry run vlc-plot-analyzer
```

Alternatively, it can be used with the (unmerged) HTTP logger from VLC for
live updates on the PTS values.

```
poetry run vlc-plot-analyzer --http --port 8080
```

Traces can then be pushed to the server with the PUT verb (one line at a time):

Example with the `curl` command:

```
curl -X PUT http://localhost:8080 -H "Content-Type: application/json" -d '<trace_line>' 
```

Traces can also be pushed from the clipboard (by clicking on the "Paste from clipboard" button or using the classic `Paste` shortcut).

## Jupyter integration

You can use [Jupyter](https://jupyter.org/) notebooks to interact with `vlc_pa` script.

To do so:

- launch `vlc_pa.py` script, load some data, and click on the `Connect to Jupyter` button
- launch `jupyter lab` from this project root directory, and execute in your notebook:

```
jupyter_prompt>>> from local import local; local.connect()
```

You can use the `linear_regression.ipynb` example to learn how to use the local connection in Jupyter:

```
<vlc_pa root dir>$ jupyter lab linear_regression.ipynb
```

This should launch a browser page locally.

## Tips

- trace files drag'n'drop is supported
- double-clicking on a plot checkbox or checkbox label will show the corresponding plot and hide every others
- if the trace file contains a large amount of data, try starting `vlc_pa.py` with the `--no-auto-select` argument and manually select the plot your want to display.
- look at all the arguments available with `poetry run vlc-plot-analyzer --help`.

## Miscellaneous

Special thanks to Shin, Jeong-Ho for the original idea and all the help on this tool!

