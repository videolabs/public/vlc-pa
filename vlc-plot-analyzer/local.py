from multiprocessing.managers import BaseManager

# These are default key and port for vlc-pa and jupyter to share information
# locally (aka on localhost).
# This is not meant to be used between two different machines.
DEFAULT_KEY=b"po12u3oh123h1"
DEFAULT_PORT=6666

class MainInterface:
    """Declares all the methods published by the local connection.
       These methods still need to be implemented on the server side."""
    def _remote_get_data(self):
        """returns the data displayed by vlc_pa in DataFrame format."""
        raise NotImplementedError()
    def _remote_add_plot(self, axis, x, y, **kwargs):
        """Add an aditional plot to vlc_pa canvas.
           Returns an [id] that can be used to remove it afterwards."""
        # Note: the basic BaseManager always return an AutoProxy object instead of the
        # real return value of the registered callable. To get a proper value
        # remotely, one simple method is to return an object that includes a
        # public method which will return the primitive value, like a list
        # (through the pop method)
        raise NotImplementedError()
    def _remote_remove_plot(self, id):
        """Remove the additional plot previously created"""
        raise NotImplementedError()

class LocalConn(BaseManager):
    """A local connection manager based on BaseManager.
       Used to proxify data and commands between vlc_pa and jupyter dashboards."""
    def __init__(self, main: MainInterface = None):
        super(LocalConn, self).__init__(address=('localhost', DEFAULT_PORT), authkey=DEFAULT_KEY)
        self.main = main
        self.register('_remote_get_data', self.main._remote_get_data if main is not None else None)
        self.register('_remote_add_plot', self.main._remote_add_plot if main is not None else None)
        self.register('_remote_remove_plot', self.main._remote_remove_plot if main is not None else None)

    def get_data(self):
        return self._remote_get_data().copy()

    def plot(self, axis, x, y, **kwargs):
        # AutoProxy[list].pop(0) will return the actual value of the 1st
        # element remotely
        return self._remote_add_plot(axis, x, y, **kwargs).pop(0)

    def remove_plot(self, id):
        self._remote_remove_plot(id)

local = LocalConn()
