import typing as T

import matplotlib
from PyQt5 import QtCore, QtWidgets, QtGui
from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar
)
from matplotlib.figure import Figure
from threading import Thread, Lock
from queue import Queue, Empty
import argparse
import textwrap
from aiohttp import web
import asyncio
import json
import os
import sys
import logging as log
import time
import numpy as np
import pandas as pd
from enum import Enum
from .local import LocalConn, MainInterface

matplotlib.use('Qt5Agg')

class VLCLogParserMode(Enum):
    KeyValueMode = 1
    JSONMode = 2

DEFAULT_PARSER_MODE=VLCLogParserMode.JSONMode

class VLCLogParser:

    def __init__(self, queue, mode=DEFAULT_PARSER_MODE):
        self.queue = queue
        if mode == VLCLogParserMode.JSONMode:
            self.parse_log_line = self.parse_json_log_line
        elif mode == VLCLogParserMode.KeyValueMode:
            self.parse_log_line = self.parse_key_value_log_line

    def get_json_label(self, j):
        label = ""
        for key in ["type", "stream", "id", "event"]:
            label += "[{}]".format(j[key]) if key in j else ""
        return label

    def parse_json_log_line(self, line):
        # Parse the json line
        try:
            line_data = json.loads(line)
        except Exception as e:
            log.debug("Cannot parse json log line: " + str(e))
            return

        if type(line_data) != dict:
            log.debug("Unexpected json log line format: " + line)
            return

        if "Timestamp" not in line_data or "Body" not in line_data:
            log.debug("Cannot find Timestamp or Body: " + line)
            return

        ts = line_data["Timestamp"]
        body = line_data["Body"]

        label = self.get_json_label(body)
        if len(label) == 0:
            log.debug("Cannot generate a valid label : " + str(body))
            return

        values = {}
        for k, v in body.items():
            if k in ["type", "stream", "id", "event"]:
                continue
            try:
                values[k] = int(v) / 10 ** 6
            except ValueError:
                try:
                    values[k] = float(v)
                except ValueError:
                    pass

        try:
            its = int(ts) / 10 ** 6
            self.queue.put((label, its, values))
            log.debug("PUSHING" + str((label, its, values)))
        except ValueError:
            log.debug(
                "Error: some timestamp are not convertible to integer: {}"
                .format(line_data)
            )

    def parse_key_value_log_line(self, line):
        """Old way to parse trace lines (key=value style)"""
        # Drop line if it's not an avstat log
        if "avstats:" not in line:
            return
        split = line.split("avstats:")[1].split()
        # find a label in line
        label = split[0]
        if "=" in split:
            log.debug("Cannot find a proper label in avstats: " + line)
            return
        log.debug("STATLINE: " + line)
        values = {}
        for chunk in split[1:]:
            if len(chunk.split("=")) == 2:
                k, v = chunk.split("=")
                try:
                    values[k] = int(v) / 10 ** 6
                except ValueError:
                    log.debug(
                        "Error: some timestamp or value are not convertible"
                        " to integer: {}"
                        .format(chunk)
                    )
                    try:
                        values[k] = float(v)
                    except ValueError:
                        log.debug(
                            "Error: some timestamp or value are not"
                            " convertible to float: {}"
                            .format(chunk)
                        )
        # "ts" aka system timestamp is mandatory
        if "ts" not in values.keys():
            log.debug(" - could not find timestamp for log: " + line)
            return
        ts = values.pop("ts")
        self.queue.put((label, ts, values))
        log.debug("PUSHING " + str((label, ts, values)))


class ReaderThread(Thread):
    def __init__(self, keep_source=False):
        super(ReaderThread, self).__init__()
        self.parser = None
        self.keep_source = keep_source
        self.source = []
    def set_parser(self, parser):
        self.parser = parser
    def get_source_filename(self):
        """returns the source filename if it is relevant"""
        return None


class InputReaderThread(ReaderThread):
    parser : VLCLogParser

    def __init__(self, filename, keep_source=False):
        super(InputReaderThread, self).__init__(keep_source=keep_source)
        self.filename = filename
        # use argparse.FileType to get a fd, to use all the universal conventions
        # (no argument or "-" -> stdin, etc.)
        parser = argparse.ArgumentParser()
        parser.add_argument('input', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
        args = parser.parse_args([filename] if filename is not None else [])
        self.fd = args.input
        self.running = True

    def run(self):
        with self.fd:
            for line in self.fd:
                if not self.running:
                    break
                self.parser.parse_log_line(line)
                if self.keep_source:
                    self.source.append(line if line.endswith('\n') else line + '\n')
        log.info("PARSING ENDED")

    def stop(self):
        self.running = False

    def get_source_filename(self):
        """returns the original filename if the source is supposed to come from
           a real file (and not a convention for getting info from a pipeline)"""
        return self.filename if self.filename != "-" else None

class BufferReaderThread(ReaderThread):
    """Parse info from a text buffer"""

    parser : VLCLogParser
    buffer : str

    def __init__(self, buffer, keep_source=False):
        super(BufferReaderThread, self).__init__(keep_source=keep_source)
        self.buffer = buffer
        self.running = True

    def run(self):
        for line in self.buffer.splitlines():
            if not self.running:
                break
            self.parser.parse_log_line(line)
            if self.keep_source:
                self.source.append(line if line.endswith('\n') else line + '\n')
        log.info("PARSING ENDED")

    def stop(self):
        self.running = False

class HTTPServerThread(ReaderThread):

    port : int
    loop : asyncio.AbstractEventLoop
    parser : VLCLogParser
    keep_source : bool

    def __init__(self, port, keep_source=False):
        super(HTTPServerThread, self).__init__(keep_source=keep_source)
        self.port = port

    def aiohttp_server(self):
        async def handle_vlc_put(request):
            while True:
                body = await request.content.readline()
                if body == b'':
                    log.info(" --- end of logging")
                    break
                line = body.decode("utf-8")
                self.parser.parse_log_line(line)
                if self.keep_source:
                    self.source.append(line if line.endswith('\n') else line + '\n')
            return web.Response(text="")
        app = web.Application()
        app.add_routes([web.put('/', handle_vlc_put)])
        runner = web.AppRunner(app)
        return runner

    def run(self):
        """Run aiohttp in a separate thread,
           to let mathplotlib alone in the main one."""
        runner = self.aiohttp_server()
        self.loop = asyncio.new_event_loop()
        self.loop.run_until_complete(runner.setup())
        site = web.TCPSite(runner, '0.0.0.0', self.port)
        self.loop.run_until_complete(site.start())
        self.loop.run_forever()

    def stop(self):
        self.loop.stop()


class SafeData:
    lock : Lock
    values: dict[str, tuple[list[T.Any], list[T.Any]]] # TODO
    values_state : int
    meta: dict[str, dict[str, T.Any]] # TODO

    def __init__(self):
        self.lock = Lock()
        # init values
        self.values = {}
        self.values_state = 0
        self.meta = {}

    def unqueue(self, queue):
        """Unqueue one item.
           Can trigger an Empty exception if the queue is empty."""
        (label, x, values_dict) = queue.get_nowait()
        with self.lock:
            # Add a "fake" event only data
            if label not in self.values:
                self.meta[label] = {"generated": True, "label": label, "variable": None}
                self.values[label] = ([], [])
            if len(self.values[label][0]) > 0 and self.values[label][0][-1] == x:
                log.debug("Data unqueue: event only data already included")
            else:
                self.values[label][0].append(x)
                self.values[label][1].append(label)
            for var, v in values_dict.items():
                name = name_from([label, var])
                if name not in self.values:
                    self.meta[name] = {"generated": False, "label": label, "variable": var}
                    self.values[name] = ([], [])
                self.values[name][0].append(x)
                self.values[name][1].append(v)
                self.values_state += 1
        queue.task_done()

    def to_dataframe(self):
        return pd.concat([pd.DataFrame({name: y}, index=x) for name, [x,y] in self.values.items()], axis=1)

    def import_dataframe(self, df):
        for column in df:
            serie = df[column].dropna()
            self.values[column] = (list(serie.index), list(serie.values))
        self.values_state += 1

    def reset(self):
        with self.lock:
            # values_state should probably not be reset, as reset is still a
            # new state of the data
            self.values = {}

def name_from(item):
    if isinstance(item, str):
        return item
    return "{} {}".format(item[0], item[1])

def autoselect_axis(ys):
    """try to select axis automatically based on y values"""
    axis = "left"
    if len(ys) > 0 and isinstance(ys[0], str):
        # Not a numerical value
        axis = "events"
    return axis

# Converts the output data from the Parser queue to data usable
# by a plot (name -> ([x1, x2, ...] [y1, y2, ...])
class QueueConsumerThread(Thread):
    queue : Queue
    data : SafeData
    running : bool
    flush : bool

    def __init__(self, queue, data):
        super(QueueConsumerThread, self).__init__()
        self.queue = queue
        self.data = data
        self.running = True
        self.flush = True

    def run(self):
        empty = False
        while self.running or (self.flush and not empty):
            try:
                self.data.unqueue(self.queue)
                empty = False
            except Empty:
                empty = True
                # let other tasks run
                time.sleep(0.000001)

    def stop(self, flush=False):
        self.flush = flush
        self.running = False

class DataProducer:
    """Generates data from various sources.
       Always includes a SafeData as output through get_data()"""
    def __init__(self):
        self.data = SafeData()
        self.source = []
    def get_data(self):
        return self.data
    def clear(self):
        self.data.reset()
    def start(self):
        raise NotImplementedError()
    def stop(self):
        raise NotImplementedError()
    def export_to_csv(self, csv):
        raise NotImplementedError()
    def get_source_filename(self):
        """returns filename only if it is relevant"""
        return None
    def write_source_to_file(self, filename):
        with open(filename, mode='w') as f:
             f.writelines(self.source)

class DataProducerFromReader(DataProducer):
    """Generates data from a ReaderThread.
       The ReaderThread instance extracts information with a LogParser, which
       pushes datae in a Queue.
       The QueueConsumerThread with then unqueue them in the SafeData instance.
    """
    def __init__(self, reader_thread):
        super(DataProducerFromReader, self).__init__()
        self.queue = Queue()
        self.parser = VLCLogParser(self.queue)
        self.reader_thread = reader_thread
        self.source = self.reader_thread.source
        self.reader_thread.set_parser(self.parser)
        self.consumer_thread = QueueConsumerThread(self.queue, self.data)

    def start(self):
        self.reader_thread.start()
        self.consumer_thread.start()

    def stop(self):
        self.reader_thread.stop()
        self.consumer_thread.stop()

    def export_to_csv(self, csv):
        self.reader_thread.join()
        self.consumer_thread.stop(flush=True)
        self.consumer_thread.join()
        self.data.to_dataframe().to_csv(csv)

    def get_source_filename(self):
        return self.reader_thread.get_source_filename()

class InputDataProducer(DataProducerFromReader):
    """DataProducer extracting data from a file."""
    def __init__(self, filename, keep_source=False):
        super(InputDataProducer, self).__init__(InputReaderThread(filename, keep_source=keep_source))

class HTTPServerDataProducer(DataProducerFromReader):
    """DataProducer extracting data pushed to a HTTP server."""
    def __init__(self, port, keep_source=False):
        super(HTTPServerDataProducer, self).__init__(HTTPServerThread(port, keep_source=keep_source))

class BufferDataProducer(DataProducerFromReader):
    """DataProducer extracting data from a text buffer."""
    def __init__(self, buffer, keep_source=False):
        super(BufferDataProducer, self).__init__(BufferReaderThread(buffer, keep_source=keep_source))

class CSVDataProducer(DataProducer):
    """DataProducer extracting data from a csv."""
    def __init__(self, csv):
        super(CSVDataProducer, self).__init__()
        df = pd.read_csv(csv, index_col=0)
        self.data.import_dataframe(df)

    def start(self):
        pass

    def stop(self):
        pass

    def export_to_csv(self, csv):
        # data is already available
        self.data.to_dataframe().to_csv(csv)

class PlotCanvas(FigureCanvas):

    def __init__(self, visible_by_default=True):
        self.fig = Figure()
        super(PlotCanvas, self).__init__(self.fig)
        self.visible_by_default = visible_by_default
        # two different plot spaces: one for 2D data, one for 1D data
        self.gs = self.fig.add_gridspec(2, 1, hspace=0, height_ratios=[3, 1])
        # Ax1 == left 2D axis
        self.ax1 = self.fig.add_subplot(self.gs[0, 0], zorder=10, picker=True)
        self.ax1.grid()
        self.ax1.set_xlabel("ts from system clock (ms)")
        self.ax1.set_ylabel("pkt value (ms)")
        self.ax1.tick_params(axis='y', labelcolor="tab:red")
        self.ax1.xaxis.set_major_formatter(
            matplotlib.ticker.StrMethodFormatter('{x:,.0f}')
        )
        self.ax1.yaxis.set_major_formatter(
            matplotlib.ticker.StrMethodFormatter('{x:,.0f}')
        )

        # Ax2 == right 2D axis
        self.ax2 = self.ax1.twinx()
        self.ax2.set_picker(True)
        self.ax2.set_zorder(11)
        self.ax2.tick_params(axis='y', labelcolor="tab:blue")
        self.ax2.set_ylabel("ts from system clock (ms)")
        self.ax2.yaxis.set_major_formatter(
            matplotlib.ticker.StrMethodFormatter('{x:,.0f}')
        )

        self.curves = {}
        self.curve_dispatch = {}
        self.curve_meta = {}
        self.auto_add = True  # add curve automatically
        self.legends_visible = True
        self.reforge_legends()

        # Add 1D curves
        self.axevents = self.fig.add_subplot(
            self.gs[1, 0], sharex=self.ax1, yticks=[], yticklabels=[],
            picker=True)
        self.axevents.grid()
        self.axevents.set_visible(False)
        self.gs.set_height_ratios([3, 0.1])

        # keep track of extra plot that could be generated through the
        # `add_extra_plot` method. stores: { id -> plot }
        self.extra_plots = {}

        # Generate annotations
        self.generate_annotations()

        self.mpl_connect("motion_notify_event", self.hover)
        self.mpl_connect('pick_event', self.pick)
        self.fig.tight_layout()

    def config(self, config):
        self.auto_add = False
        for item in config["left"]:
            name = name_from(item)
            self.curve_dispatch[name] = "left"
            self.curve_meta[name] = {"label":item[0], "variable":item[1]}
        for item in config["right"]:
            name = name_from(item)
            self.curve_dispatch[name] = "right"
            self.curve_meta[name] = {"label":item[0], "variable":item[1]}
        for name in config["events"]:
            self.curve_dispatch[name] = "events"
            self.curve_meta[name] = {"label":name}

    def export_config(self):
        """Convert current curve dispatch/presence to a proper config dictionnary"""
        config = {
            "comments": [
                "Data are what you want to be displayed in graphs",
                "Three plots are available: left axis (2D), right axis (2D), events (1D)",
                "Left and Right plots are traditional scatter/line plots for two-dimensional data",
                "Events are one dimensional data: they are displayed as points based on timestamp"],
            "left": [],
            "right": [],
            "events": []
        }
        for name, dispatch in self.curve_dispatch.items():
            log.info(f"config {name}")
            meta = self.curve_meta[name]
            if meta["variable"] is not None:
                config[dispatch].append([meta["label"], meta["variable"]])
            else:
                config[dispatch].append(meta["label"])
        return config

    def add_curve(self, axis, name, x, y, metadata=None, visible=True, color=None):
        xs = np.array(x)
        if axis == "left":
            ys = np.array(y)
            [self.curves[name]] = self.ax1.plot(
                xs, ys, marker='o', label=name, markersize=3, zorder=10,
                visible=visible, color=color)
        elif axis == "right":
            ys = np.array(y)
            [self.curves[name]] = self.ax2.plot(
                xs, ys, marker='x', label=name, markersize=3, zorder=11,
                visible=visible, color=color)
        elif axis == "events":
            ys = np.array(y, dtype='U')  # force string type
            # make axevents visisble
            self.axevents.set_visible(True)
            self.gs.set_height_ratios([3, 1])
            [self.curves[name]] = self.axevents.plot(
                xs, ys, marker='o', label=name, markersize=3, zorder=10,
                visible=visible, linestyle='None', color=color)
            event_labels = self.get_event_labels()
            self.axevents.set_yticks(range(len(event_labels)))
            self.axevents.set_yticklabels(event_labels)
        if metadata is not None:
            self.curve_meta[name] = metadata
        self.reforge_legends()

    def remove_curve(self, name, redraw=True):
        dispatch = self.curve_dispatch.pop(name)
        self.curve_meta.pop(name)
        to_remove_curve = self.curves.pop(name)
        to_remove_curve.remove()
        if dispatch == "events":
            # for this particular axis, it is simpler to regenerate every plot
            self.axevents.clear()
            self.axevents.grid()
            self.generate_annotations()
            for name, dis in self.curve_dispatch.items():
                if name in self.curves and dis == "events":
                    current_curve = self.curves[name]
                    self.add_curve("events", name, current_curve.get_xdata(), current_curve.get_ydata(),
                                   visible=current_curve.get_visible(), color=current_curve.get_color())
        if redraw:
            self.reforge_legends()
            self.draw_idle()

    def move_curve_to_axis(self, name, axis, redraw=True):
        old_curve = self.curves[name]
        old_meta = self.curve_meta[name]
        self.remove_curve(name)
        self.curve_dispatch[name] = axis
        self.add_curve(axis, name, old_curve.get_xdata(), old_curve.get_ydata(),
                       metadata=old_meta, visible=old_curve.get_visible(),
                       color=old_curve.get_color())
        if redraw:
            self.reforge_legends()
            self.draw_idle()

    def get_event_labels(self):
        return [name for name in self.curves.keys()
                if name in self.curve_dispatch.keys()
                and self.curve_dispatch[name] == "events"]

    def generate_annotations(self):
        options = dict( text="", xy=(0, 0), xycoords='figure pixels',
            xytext=(20, 20), textcoords="offset points",
            bbox=dict(boxstyle="round", fc="w"),
            arrowprops=dict(arrowstyle="->"), zorder=1000, visible=False)
        self.annot1 = self.ax1.annotate(**options)
        self.annot2 = self.ax2.annotate(**options)
        self.annotevent = self.axevents.annotate(**options)

    def remove_all_curves(self, redraw=True):
        self.ax1.clear()
        self.ax1.grid()
        self.ax2.clear()
        self.axevents.clear()
        self.axevents.grid()
        self.curves = {}
        self.curve_dispatch = {}
        self.curve_meta = {}
        # apparently, clear functions remove the annotations, so we have to recreate them
        self.generate_annotations()
        if redraw:
            self.reforge_legends()
            self.draw_idle()

    def update_plot(self, name, x, y, meta):
        """Update the <name> plot with the corresponding data.
           Returns True if the plot is updated for real,
           And False if ignored (due to not being auto_add for example)"""
        result = True
        if name not in self.curves:
            if self.auto_add:
                axis = autoselect_axis(y)
                self.curve_dispatch[name] = axis
                self.add_curve(axis, name, x, y, metadata=meta, visible=self.visible_by_default)
            elif not self.auto_add and name in self.curve_dispatch:
                self.add_curve(self.curve_dispatch[name], name, x, y,
                               metadata=meta, visible=self.visible_by_default)
            else:
                result = False
        else:
            xs = np.array(x)
            ys = np.array(y)
            self.curves[name].set_data(xs, ys)
        return result

    def toggle_legend(self):
        self.legends_visible = not self.legends_visible
        self.reforge_legends()
        self.draw_idle()

    def reforge_legends(self):
        for axis, loc in [(self.ax1, 'upper left'), (self.ax2, 'upper right')]:
            visible = self.legends_visible
            _, labels = axis.get_legend_handles_labels()
            if len(labels) == 0:
                legend = axis.legend([''], loc=loc)
                legend.set_visible(False)
            else:
                legend = axis.legend(loc=loc)
                legend.set_visible(visible)
            legend.set_zorder(100)

    def get_points_at(self, event):
        """retrieve curve points under given event.
           returns the dictionary:
           <curve_name> -> [ point_indexes ]"""
        points = {}
        for name, curve in self.curves.items():
            if not curve.get_visible():
                continue
            cont, ind = curve.contains(event)
            if cont and "ind" in ind.keys():
                points[name] = ind["ind"]
        return points

    def pick(self, event):
        points = self.get_points_at(event.mouseevent)
        if len(points) == 0:
            return
        message_box = QtWidgets.QMessageBox()
        message_box.setWindowTitle("Points")
        text = "<table>"
        for name, idx in points.items():
            # only take the first point for each curve
            curve = self.curves[name]
            color = curve.get_color()
            xs, ys = curve.get_xdata(), curve.get_ydata()
            text += f"<tr><td style='color:{color};font-weight:bold;'>{name}</td><td>( {xs[idx[0]]} , {ys[idx[0]]} )</td></tr>"
        text += "</table>"
        message_box.setText(text)
        message_box.exec();

    def update_annot(self, annot, x, y, points):
        annot.xy = (x, y)
        textlines = []
        for name, idx in points.items():
            xs, ys = self.curves[name].get_xdata(), self.curves[name].get_ydata()
            # only show the first point
            textlines.append("{}: ({}, {})".format(name, xs[idx[0]], ys[idx[0]]))
        annot.set_text("\n".join(textlines))
        annot.get_bbox_patch().set_facecolor('w')

    def hover(self, event):
        annotMap = {self.ax1: self.annot1,
                    self.ax2: self.annot2,
                    self.axevents: self.annotevent}
        if event.inaxes is None:
            for an in annotMap.values():
                an.set_visible(False)
            self.draw_idle()
            return
        points = self.get_points_at(event)
        if len(points) == 0:
            return
        for axis, annot in annotMap.items():
            if event.inaxes != axis:
                annot.set_visible(False)
        annot = annotMap[event.inaxes]
        self.update_annot(annot, event.x, event.y, points)
        annot.set_visible(True)
        self.draw_idle()

    def rescale(self, redraw=True):
        self.ax1.relim(visible_only=True)
        self.ax2.relim(visible_only=True)
        self.axevents.relim(visible_only=True)
        self.ax1.set_autoscale_on(True)
        self.ax2.set_autoscale_on(True)
        self.axevents.set_autoscale_on(True)
        self.ax1.autoscale_view()
        self.ax2.autoscale_view()
        self.axevents.autoscale_view()
        if redraw:
            self.draw_idle()

    def scale_to_curve(self, name):
        if name not in self.curves.keys():
            log.debug(f"Curve {name} not found, abort scaling")
            return
        dispatch = self.curve_dispatch[name]
        if dispatch == "left":
            plot = self.ax1
        elif dispatch == "right":
            plot = self.ax2
        else:
            plot = self.axevents
        x, y = self.curves[name].get_data()
        xmin, xmax = np.amin(x), np.amax(x)
        # add some margin
        margin = 0.05 # 5%
        xlength = xmax - xmin
        xmin = xmin - xlength * margin
        xmax = xmax + xlength * margin
        plot.set_xlim(xmin, xmax)
        if dispatch in ["left", "right"]:
            ymin, ymax = np.amin(y), np.amax(y)
            ylength = ymax - ymin
            ymin = ymin - ylength * margin
            ymax = ymax + ylength * margin
            plot.set_ylim(ymin, ymax)
        self.draw_idle()

    def set_visibility(self, name, check, redraw=True):
        log.debug(f"Set visibility: {name} to {check}, redraw: {redraw} ")
        curve = self.curves[name]
        curve.set_visible(check)
        if check:
            curve.set_label(name)
        else:
            curve.set_label('_Hidden')
        if redraw:
            self.reforge_legends()
            self.draw_idle()

    def set_alpha(self, name, alpha, others_alpha=None):
        for n, curve in self.curves.items():
            if n == name and alpha is not None:
                curve.set(alpha=alpha)
            elif n != name and others_alpha is not None:
                curve.set(alpha=others_alpha)
        self.draw_idle()

    def add_extra_plot(self, axis, x, y,  **kwargs):
        axMap = {"left": self.ax1, "right": self.ax2}
        if axis not in axMap.keys():
            raise ValueError(f"{axis} axis not found")
        ax = axMap[axis]
        [new_plot] = ax.plot(x, y, **kwargs)
        key = id(new_plot)
        self.extra_plots[key] = new_plot
        self.reforge_legends()
        self.draw_idle()
        return key

    def remove_extra_plot(self, id):
        if id not in self.extra_plots.keys():
            raise ValueError(f"cannot find extra plot {id}")
        plot = self.extra_plots.pop(id)
        plot.remove()
        self.reforge_legends()
        self.draw_idle()

def create_hline():
    line = QtWidgets.QFrame()
    line.setFrameShape(QtWidgets.QFrame.HLine)
    return line

class ZoomButton(QtWidgets.QToolButton):
    def __init__(self, *args, **kwargs):
        super(QtWidgets.QToolButton, self).__init__(*args, **kwargs)
        self.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        self.setIcon(QtGui.QIcon(os.path.join(matplotlib.get_data_path(),"images/zoom_to_rect_large.png")))
        self.setCheckable(False)

class CheckBoxWithDoubleClickAndMouseOver(QtWidgets.QCheckBox):

    def __init__(self, *args, **kwargs):
        super(CheckBoxWithDoubleClickAndMouseOver, self).__init__(*args, **kwargs)
        self.double_clicked = None
        self.enter_callback = None
        self.leave_callback = None

    @T.override
    def mouseDoubleClickEvent(self, a0):
        if self.double_clicked is not None:
            self.double_clicked(a0)

    @T.override
    def enterEvent(self, a0):
        if self.isChecked() and self.isEnabled() and self.enter_callback is not None:
            self.enter_callback(a0)
        super(CheckBoxWithDoubleClickAndMouseOver, self).enterEvent(a0)

    @T.override
    def leaveEvent(self, a0):
        if self.isChecked() and self.isEnabled() and self.leave_callback is not None:
            self.leave_callback(a0)
        super(CheckBoxWithDoubleClickAndMouseOver, self).leaveEvent(a0)


class CurveBox:
    """UI box representing the curve present in some canvas.
       The box includes the curve label, point counter, actions related
       to the curve like zoom, etc."""

    checkbox : CheckBoxWithDoubleClickAndMouseOver
    gbox : T.Optional[QtWidgets.QGroupBox]

    def __init__(self, parent, category, name, checked=True,
                 disabled_at_start=True, start_counter=0,
                 checkbox_callback=None, double_click_callback=None,
                 mouseover_enter_callback=None, mouseover_leave_callback=None,
                 zoom_callback=None, menu_button=None):
        self.parent = parent
        self.category = category 
        self.name = name
        # checkbox
        self.checkbox = CheckBoxWithDoubleClickAndMouseOver(name)
        self.checkbox.setChecked(checked)
        if checkbox_callback is None:
            checkbox_callback = self.togglevisibility
        if mouseover_enter_callback is None:
            mouseover_enter_callback = lambda event : self.parent.update_box_mouseover_name(name)
        if mouseover_leave_callback is None:
            mouseover_leave_callback = lambda event : self.parent.update_box_mouseover_name(None)
        if double_click_callback is None:
            double_click_callback = lambda event : self.parent.set_visibility_all_but_one(name, True)
        self.checkbox.stateChanged.connect(checkbox_callback)
        self.checkbox.double_clicked = double_click_callback
        self.checkbox.enter_callback = mouseover_enter_callback
        self.checkbox.leave_callback = mouseover_leave_callback
        self.checkbox.setDisabled(disabled_at_start)
        # point counter
        counter_label = str(start_counter) if start_counter is not None else ''
        self.counter = QtWidgets.QLabel()
        # button group
        self.gbox = QtWidgets.QGroupBox()
        self.gbox.setStyleSheet("QGroupBox{border:0;}")
        layout = QtWidgets.QHBoxLayout(self.gbox)
        layout.setContentsMargins(0,0,0,0)
        layout.setSpacing(0)
        # Zoom button
        zoom_button = ZoomButton()
        zoom_button.setText(name)
        if zoom_callback is None:
            zoom_callback = self.scale_to_curve
        zoom_button.clicked.connect(zoom_callback)
        zoom_button.setDisabled(disabled_at_start)
        layout.addWidget(zoom_button)
        # Menu button
        if menu_button is None:
            menu = QtWidgets.QMenu()
            menu.addAction('Delete', self.remove)
            if self.category in ['left', 'right']:
                menu.addAction('Move to the other axis', self.move)
            menu_button = QtWidgets.QToolButton()
            menu_button.setMenu(menu);
            menu_button.setPopupMode(QtWidgets.QToolButton.InstantPopup)
            menu_button.setArrowType(QtCore.Qt.ArrowType.DownArrow)
            menu_button.setStyleSheet("QToolButton::menu-indicator{ image: none; };")
            menu_button.setDisabled(disabled_at_start)
        layout.addWidget(menu_button)
        # color
        self.color = None
        self.update_color()

    def set_color(self, color):
        self.color = color
        if self.color is not None:
            self.checkbox.setStyleSheet("color: {}".format(color))

    def update_color(self):
        if self.color is None and self.name in self.parent.canvas.curves:
            self.color = self.parent.canvas.curves[self.name].get_color()
            self.checkbox.setStyleSheet("color: {}".format(self.color))

    def scale_to_curve(self):
        log.debug(f"Adjusting zoom to curve: {self.name}")
        self.parent.canvas.scale_to_curve(self.name)

    def togglevisibility(self):
        self.parent.canvas.set_visibility(self.name, self.checkbox.isChecked())

    def set_visibility(self, visible, redraw=True):
        """force checkbox and curve visibility without triggering the stateChanged event"""
        self.checkbox.blockSignals(True)
        self.checkbox.setChecked(visible)
        self.checkbox.blockSignals(False)
        self.parent.canvas.set_visibility(self.name, visible, redraw=redraw)

    def put_in_layout(self, layout, line):
        """put the box in the given grid layout at some line number"""
        layout.addWidget(self.checkbox, line, 0)
        layout.addWidget(self.counter, line, 1, alignment=QtCore.Qt.AlignRight)
        layout.addWidget(self.gbox, line, 2, alignment=QtCore.Qt.AlignRight)

    def remove_from_layout(self, layout):
        layout.removeWidget(self.checkbox)
        layout.removeWidget(self.counter)
        layout.removeWidget(self.gbox)

    def clear(self):
        self.checkbox = None
        self.counter = None
        self.gbox = None

    def move(self):
        if self.category == "events":
            return
        new_category = "left" if self.category == "right" else "right"
        log.info(f"Move {self.name} to the axis {new_category}")
        self.parent.move_curve(self.name, new_category)

    def remove(self):
        """Use a timer to schedule the curve removal ASAP.
           This will prevent the box to try to remove itself and fail."""
        remove_curve_timer = QtCore.QTimer(self.parent)
        remove_curve_timer.setInterval(0)
        remove_curve_timer.setSingleShot(True)
        remove_curve_timer.timeout.connect(lambda: self.parent.remove_curve(self.name, notify_ignore=True))
        remove_curve_timer.start()

    def enable(self):
        self.checkbox.setDisabled(False)
        for c in self.gbox.children():
            if isinstance(c, QtWidgets.QWidget):
                c.setDisabled(False)

class MainWindow(QtWidgets.QMainWindow, MainInterface):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

    def initialize(self, config, producer, visible_by_default=True,
                   auto_reload=False, keep_source=False):
        self.original_config = config
        self.keep_source = keep_source
        self.visible_by_default = visible_by_default
        self.producer = producer
        self.auto_reload = auto_reload
        self.last_values_state = -1
        self.auto_add = True
        self.keep_source = keep_source
        self.local_server = None

        # Init plot canvas
        self.canvas = PlotCanvas(self.visible_by_default)

        # Create toolbar, passing canvas as first parameter,
        # parent (self, the MainWindow) as second.
        navtoolbar = NavigationToolbar(self.canvas, self)
        actions_to_keep = ['Pan', 'Zoom']
        for action in navtoolbar.actions():
            if action.text() not in actions_to_keep:
                navtoolbar.removeAction(action)
        paste = navtoolbar.addAction('Paste from clipboard', self.paste_from_clipboard)
        paste.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_FileDialogDetailedView))
        open = navtoolbar.addAction('Open file', self.open_file)
        open.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_FileDialogStart))
        save = navtoolbar.addAction('Save source', self.save_source)
        save.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_DialogSaveButton))
        reload = navtoolbar.addAction('Clear/Reload', self.reload)
        reload.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_BrowserReload))
        toggle_legend = navtoolbar.addAction('Toggle Legend', self.canvas.toggle_legend)
        toggle_legend.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_MessageBoxInformation))
        save_config = navtoolbar.addAction('Save config', self.save_config)
        save_config.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_DriveFDIcon))
        jupyter = navtoolbar.addAction('Connect to Jupyter', self.local_connection)
        jupyter.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_DesktopIcon))
        spacer = QtWidgets.QWidget();
        spacer.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding);
        navtoolbar.addWidget(spacer)
        quit = navtoolbar.addAction('Quit', self.close)
        quit.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_BrowserStop))
        self.addToolBar(navtoolbar)

        # shortcuts
        self.paste_shortcut = QtWidgets.QShortcut(QtGui.QKeySequence.Paste, self)
        self.paste_shortcut.activated.connect(self.paste_from_clipboard)
        self.save_shortcut = QtWidgets.QShortcut(QtGui.QKeySequence.Save, self)
        self.save_shortcut.activated.connect(self.save_source)
        self.open_shortcut = QtWidgets.QShortcut(QtGui.QKeySequence.Open, self)
        self.open_shortcut.activated.connect(self.open_file)

        layout = QtWidgets.QGridLayout()
        layout.addWidget(self.canvas, 0, 0)

        # add plot list dock
        dock = QtWidgets.QDockWidget("Plots")
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, dock)
        scroll = QtWidgets.QScrollArea()
        dock.setWidget(scroll)
        content = QtWidgets.QWidget()
        scroll.setWidget(content)
        scroll.setWidgetResizable(True)
        scroll.setMinimumWidth(350)
        vlayout = QtWidgets.QVBoxLayout(content)
        self.boxes = {}
        self.box_widgets = {}

        # Create "All" group
        g = QtWidgets.QGroupBox("All")
        vlayout.addWidget(g)
        boxlist = QtWidgets.QGridLayout(g)
        boxlist.setVerticalSpacing(0)
        self.warning_button = QtWidgets.QToolButton()
        self.warning_button.setText("Some Data is Hidden")
        self.warning_button.setIcon(self.style().standardIcon(QtWidgets.QStyle.SP_MessageBoxWarning))
        self.warning_button.clicked.connect(self.show_ignored_dialog)
        self.warning_button.setDisabled(True)
        box = CurveBox(self, '', "All Curves", checked=True,
                       disabled_at_start=False,
                       checkbox_callback=self.togglevisibilityall,
                       double_click_callback=lambda *args:None,
                       mouseover_enter_callback=lambda *args:None,
                       zoom_callback=lambda: self.canvas.rescale(redraw=True),
                       start_counter=None, menu_button=self.warning_button)
        box.put_in_layout(boxlist, 0)

        for cat in ["left", "right", "events"]:
            g = QtWidgets.QGroupBox(cat)
            vlayout.addWidget(g)
            boxlist = QtWidgets.QGridLayout(g)
            boxlist.setVerticalSpacing(0)
            self.box_widgets[cat] = (g, boxlist)
            g.setVisible(False)
        vlayout.insertStretch(-1, 1)

        # Setup a timer for mouseover delay/reset on boxes
        self.box_mouseover_timer = QtCore.QTimer()
        self.box_mouseover_timer.setInterval(100)
        self.box_mouseover_timer.setSingleShot(True)
        self.box_mouseover_timer.timeout.connect(self.box_mouseover_timer_end)
        self.box_mouseover_name = None

        if self.original_config is not None:
            self.canvas.config(self.original_config)
            self.config(self.original_config)
        # if some configuration is provided, some data could be ignored/hidden.
        # Let's track them
        self.ignored_data = []

        # Create a placeholder widget to hold our toolbar and canvas.
        widget = QtWidgets.QWidget()
        widget.setLayout(layout)
        self.setCentralWidget(widget)

        # We need to store a reference to the plotted line
        # somewhere, so we can apply the new data to it.
        self.update()
        self.show()

        # Setup a timer to trigger the redraw by calling update.
        self.timer = QtCore.QTimer()
        self.timer.setInterval(16)  # no more than 60fps
        self.timer.timeout.connect(self.update)
        self.timer.start()

        # Add drag'n'drop support for urls (files)
        self.setAcceptDrops(True)

        self.file_watcher = None
        if self.auto_reload and self.producer.get_source_filename() != None:
            self.update_filesystem_watcher(self.producer.get_source_filename())

        self.thread_manager = QtCore.QThreadPool()

    def save_source(self):
        filename, _ = QtWidgets.QFileDialog.getSaveFileName(caption="Save source as...")
        if filename != '':
            log.info(f"Saving source in filename: {filename}")
            self.producer.write_source_to_file(filename)

    def open_file(self):
        filename, _ = QtWidgets.QFileDialog.getOpenFileName(caption="Open trace file")
        if filename != '':
            self.reload(new_filename=filename)

    def update_filesystem_watcher(self, filename):
        if self.file_watcher is not None:
            if filename is not None and os.path.abspath(filename) in self.file_watcher.files():
                log.debug(f"{filename} already watched, not updating")
                return
            # clear file_watcher
            self.file_watcher.removePaths(self.file_watcher.files())
            self.file_watcher.blockSignals(True)
            self.file_watcher = None
        if filename is not None:
            self.file_watcher = QtCore.QFileSystemWatcher([os.path.abspath(filename)])
            self.file_watcher.fileChanged.connect(lambda event: self.reload())

    def update_box_mouseover_name(self, name):
        """Update the name of the box that has been over mouse.
           Reset the timer: the real action will be performed at the end of the delay."""
        self.box_mouseover_name = name
        self.box_mouseover_timer.start()

    def box_mouseover_timer_end(self):
        """Trigger the real action at the end of the box mouseover delay"""
        if self.box_mouseover_name is None:
            self.canvas.set_alpha(self.box_mouseover_name, 1.0, 1.0)
        else:
            self.canvas.set_alpha(self.box_mouseover_name, 1.0, 0.1)
        self.canvas.reforge_legends()
        self.canvas.draw_idle()

    def config(self, config):
        self.auto_add = False
        for cat in ["left", "right", "events"]:
            if cat not in config or len(config[cat]) == 0:
                continue
            for plot in config[cat]:
                name = name_from(plot)
                self.add_box(cat, name, self.visible_by_default)

    def add_box(self, cat, name, visible_by_default=True):
        group, boxlist = self.box_widgets[cat]
        curve_box = CurveBox(self, cat, name, visible_by_default)
        self.boxes[name] = curve_box
        i = boxlist.rowCount()
        curve_box.put_in_layout(boxlist, i)
        if not group.isVisible():
            group.setVisible(True)

    def add_ignored_data(self, name):
        """keep track that some data have been ignored/hidden
           (due to some configuration/not auto-adding curves)"""
        self.ignored_data.append(name)
        self.warning_button.setDisabled(False)

    def clear_ignored_data(self):
        self.ignored_data = []
        self.warning_button.setDisabled(True)

    def show_ignored_dialog(self):
        """Display data that has been ignored/hidden"""
        message_box = QtWidgets.QMessageBox()
        message_box.setWindowTitle("The following Data is not displayed")
        message_box.setText("\n".join(self.ignored_data));
        message_box.exec();

    def resizeEvent(self, event):
        QtWidgets.QMainWindow.resizeEvent(self, event)
        self.canvas.fig.tight_layout()

    def closeEvent(self, event):
        self.producer.stop()
        if self.local_server is not None:
            self.local_server.stop_event.set()
        event.accept()

    def togglevisibilityall(self):
        b = self.sender()
        check = b.isChecked()
        for name in self.canvas.curves:
            self.boxes[name].set_visibility(check, redraw=False)
        self.canvas.reforge_legends()
        self.canvas.draw_idle()

    def set_visibility_all_but_one(self, selection, status):
        for name in self.canvas.curves:
            check = status if name == selection else not status
            self.boxes[name].set_visibility(check, redraw=False)
        self.canvas.reforge_legends()
        self.canvas.draw_idle()

    def update_counter(self, name, x, y):
        """Update the UI counter"""
        if name not in self.boxes:
            if not self.auto_add:
                return
            self.add_box(autoselect_axis(y), name, self.visible_by_default)
        curve_box = self.boxes[name]
        if len(x) > 0:
            curve_box.enable()
        curve_box.counter.setText(str(len(x)))
        curve_box.update_color()

    def update(self):
        data = self.producer.get_data()
        with data.lock:
            if self.last_values_state != data.values_state:
                for name in data.values:
                    [x, y] = data.values[name]
                    meta = data.meta[name]
                    updated = self.canvas.update_plot(name, x, y, meta)
                    if not updated:
                        self.add_ignored_data(name)
                    self.update_counter(name, x, y)
                self.canvas.rescale(redraw=True)
                self.last_values_state = data.values_state

    def clear_curves(self):
        """Remove all curves and related UI items."""
        self.canvas.remove_all_curves(redraw=True)
        for name in [box.name for box in self.boxes.values()]:
            self.remove_box(name)
        self.clear_ignored_data()
        if self.original_config is not None:
            # reconfigure == recreate the boxes and dispatch
            self.canvas.config(self.original_config)
            self.config(self.original_config)

    def remove_curve(self, name, notify_ignore=False):
        """Delete curve and the related box"""
        self.remove_box(name)
        if name in self.canvas.curves:
            self.canvas.remove_curve(name, redraw=True)
        if notify_ignore:
            self.add_ignored_data(name)

    def remove_box(self, name):
        box = self.boxes.pop(name)
        group, boxlist = self.box_widgets[box.category]
        box.remove_from_layout(boxlist)
        box.clear()

    def move_curve(self, name, category):
        self.canvas.move_curve_to_axis(name, category, redraw=True)
        box = self.boxes[name]
        _, old_boxlist = self.box_widgets[box.category]
        box.remove_from_layout(old_boxlist)
        group, boxlist = self.box_widgets[category]
        i = boxlist.rowCount()
        box.put_in_layout(boxlist, i)
        box.category = category
        if not group.isVisible():
            group.setVisible(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        filename = None
        for url in event.mimeData().urls():
            if os.path.exists(url.toLocalFile()):
                filename = url.toLocalFile()
                break
        if filename is None:
            return
        log.info(f"File drop detected: {filename}")
        self.reload(new_filename=filename)

    def paste_from_clipboard(self):
        clipboard = QtGui.QGuiApplication.clipboard()
        self.reload(new_clipboard=clipboard.text())

    def reload(self, new_filename=None, new_clipboard=None):
        """reload (if possible) or reset the data source"""
        self.clear_curves()
        self.producer.stop()
        self.producer.clear()
        self.last_values_state = -1
        if new_clipboard is None and new_filename is None and self.producer.get_source_filename() is not None:
            # If the source comes from a real file, recreate the producer
            new_filename = self.producer.get_source_filename()
        if self.auto_reload:
            self.update_filesystem_watcher(new_filename)
        if new_clipboard is not None:
            log.info("reloading from clipboard")
            self.producer = BufferDataProducer(new_clipboard, keep_source=self.keep_source)
        elif new_filename is not None:
            log.info(f"reloading with a file: {new_filename}")
            self.producer = InputDataProducer(new_filename, keep_source=self.keep_source)
        else:
            # do not relaunch producer
            return
        self.producer.start()

    def save_config(self):
        filename, _ = QtWidgets.QFileDialog.getSaveFileName(caption="Save config file as...")
        if filename != '':
            log.info(f"Saving config file in filename: {filename}")
            config = self.canvas.export_config()
            js = json.dumps(config, indent=2)
            with open(filename, 'w') as f:
                f.write(js)
            self.config = config
            self.auto_add = False

    def local_connection(self):
        if self.local_server is not None:
            return
        local = LocalConn(self)
        self.local_server = local.get_server()
        self.thread_manager.start(self._serve_forever)

    def _serve_forever(self):
        log.info("Starting local connection")
        self.local_server.serve_forever()
        log.info("Stopping local connection")

    def _remote_get_data(self):
        """"Implements MainInterface.get_data"""
        return self.producer.get_data().to_dataframe()

    def _remote_add_plot(self, axis, x, y, **kwargs):
        """"Implements MainInterface.plot"""
        # return a single-element list to use the AutoProxy easily
        return [self.canvas.add_extra_plot(axis, x, y,  **kwargs)]

    def _remote_remove_plot(self, id):
        """"Implements MainInterface.remove_plot"""
        self.canvas.remove_extra_plot(id)

def main():
    # Parse arguments
    args_parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description=textwrap.dedent("""\
            Generate interactive plots based on VLC stat logs.

            Logs will be read from either stdin (default), input file or HTTP stream."""))
    args_parser.add_argument(
        'input', nargs='?', type=str, help="VLC log file")
    args_parser.add_argument(
        '--http', action='store_true',
        help="Launch a HTTP server and listen to the incoming logs"
    )
    args_parser.add_argument(
        '--port', type=int, default=8080, help="HTTP server listen port")
    args_parser.add_argument(
        '--log', type=str, default="INFO", help="Log Verbosity")
    args_parser.add_argument(
        '--config', type=str, default="", help="specify the configuration file to use")
    args_parser.add_argument(
        '--export-csv', type=str, default="", help="export to CSV file")
    args_parser.add_argument(
        '--import-csv', type=str, default="", help="import data from a CSV file")
    args_parser.add_argument(
        '--auto-select', action=argparse.BooleanOptionalAction, default=True,
        help="select every plot to display at startup automatically (enabled by default)")
    args_parser.add_argument(
        '--auto-reload', action=argparse.BooleanOptionalAction, default=False,
        help="automatically reload the source file if it changes (disabled by default)")
    args_parser.add_argument(
        '--keep-source', action=argparse.BooleanOptionalAction, default=False,
        help="keep source traces in memory (to be able to save them later)")
    args = args_parser.parse_args()
    # Configure logging
    num_log_level = getattr(log, args.log.upper(), None)
    if not isinstance(num_log_level, int):
        raise ValueError('Invalid log level: %s' % num_log_level)
    log.basicConfig(level=num_log_level)
    # load config json file
    config = None
    config_filename = args.config
    if config_filename == "":
        config_filename = "config.json"
    if os.path.isfile(config_filename):
        with open(config_filename, "r") as f:
            config = json.load(f)
    producer = None
    if args.import_csv != "":
        producer = CSVDataProducer(args.import_csv)
    elif args.http:
        producer = HTTPServerDataProducer(args.port, keep_source=args.keep_source)
    else:
        producer = InputDataProducer(args.input, keep_source=args.keep_source)
    producer.start()
    if args.export_csv != "":
        # do not launch application, export data instead
        # wait for reader_thread to stop
        producer.export_to_csv(args.export_csv)
        sys.exit(0)
    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.initialize(config, producer,
                 visible_by_default=args.auto_select,
                 auto_reload=args.auto_reload,
                 keep_source=args.keep_source)
    app.exec_()

if __name__ == '__main__':
    main()
